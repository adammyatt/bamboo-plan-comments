package com.pronetbeans.bamboo.plancomments;

import com.atlassian.bamboo.comment.Comment;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Adam Myatt
 */
public class PlanCommentsDisplayPlugin extends BambooActionSupport {

    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(PlanCommentsDisplayPlugin.class);
    private String commentHtml;
    private ResultsSummaryManager resultssummarymanager;
    private String planKey;

    @Override
    public String doInput() throws Exception {

        try {
            log.info("PlanCommentsDisplayPlugin : planKey : " + planKey);
            StringBuilder sb = new StringBuilder(10000);

            String currentActionUrl = getNavigationUrl();
            String planKeyExtracted = currentActionUrl.substring(currentActionUrl.indexOf("browse/") + "browse/".length(), currentActionUrl.indexOf("/plancomments.action"));

            planKey = planKeyExtracted;

            Plan plan = getPlanManager().getPlanByKey(planKey);
            log.info("Plan.key : " + plan.getBuildName());
            List<ResultsSummary> buildresults = resultssummarymanager.getResultSummariesForPlan(plan, 0, 0);
            int curBuildNum = 0;
            for (int i = 0; i < buildresults.size(); i++) {
                ResultsSummary build = buildresults.get(i);
                int buildNum = build.getBuildNumber();
                List<Comment> comments = build.getComments();

                if(comments == null || comments.isEmpty()) {
                    continue;
                }
                if (curBuildNum != buildNum) {
                    sb.append("<a href=\"/browse/");
                    sb.append(planKey);
                    sb.append("-");
                    sb.append(buildNum);
                    sb.append("\">#");
                    sb.append(buildNum);
                    sb.append("</a><br><blockquote>");
                }


                for (int j = 0; j < comments.size(); j++) {
                    Comment comment = comments.get(j);

                    String commentText = comment.getContent();
                    Date createdDate = comment.getCreationDate();
                    String userFullName = comment.getUser().getFullName();
                    String username = comment.getUserName();

                    sb.append("<a href=\"/browse/user/");
                    sb.append(username);
                    sb.append("\">");
                    sb.append(userFullName);
                    sb.append("</a> ");
                    sb.append(createdDate);
                    sb.append("<br>");
                    sb.append(commentText);
                    sb.append("<br><br>");
                }
            
                if (curBuildNum != buildNum) {
                    sb.append("</blockquote>");
                    curBuildNum = buildNum;
                    
                }                
                

                
            }

            commentHtml = sb.toString();
        } catch (Exception e) {
            log.error("Error : " + e.getMessage());
            StackTraceElement[] errors = e.getStackTrace();
            for (int i = 0; i < errors.length; i++) {
                StackTraceElement stackTraceElement = errors[i];
                log.error("   " + stackTraceElement.getFileName() + "." + stackTraceElement.getMethodName() + " at " + stackTraceElement.getLineNumber());
            }
            e.printStackTrace();
        }

        return INPUT;
    }

    @Override
    public String doExecute() throws Exception {


        try {
        } catch (Exception e) {
            log.error("Error : " + e.getMessage());
            StackTraceElement[] errors = e.getStackTrace();
            for (int i = 0; i < errors.length; i++) {
                StackTraceElement stackTraceElement = errors[i];
                log.error("   " + stackTraceElement.getFileName() + "." + stackTraceElement.getMethodName() + " at " + stackTraceElement.getLineNumber());
            }
            e.printStackTrace();
        }

        return SUCCESS;
    }

    /**
     * @return the resultssummarymanager
     */
    public ResultsSummaryManager getResultssummarymanager() {
        return resultssummarymanager;
    }

    /**
     * @param resultssummarymanager the resultssummarymanager to set
     */
    public void setResultssummarymanager(ResultsSummaryManager resultssummarymanager) {
        this.resultssummarymanager = resultssummarymanager;
    }

    /**
     * @return the commentHtml
     */
    public String getCommentHtml() {
        return commentHtml;
    }

    /**
     * @param commentHtml the commentHtml to set
     */
    public void setCommentHtml(String commentHtml) {
        this.commentHtml = commentHtml;
    }

    /**
     * @return the planKey
     */
    public String getPlanKey() {
        return planKey;
    }

    /**
     * @param planKey the planKey to set
     */
    public void setPlanKey(String planKey) {
        this.planKey = planKey;
    }
}
